import 'package:flutter/material.dart';
import 'package:pizza_pos/helpers/responsiveness.dart';
import 'package:pizza_pos/widgets/large_screen_admin.dart';
import 'package:pizza_pos/widgets/side_menu.dart';
import 'package:pizza_pos/widgets/small_screen_admin.dart';
import 'package:pizza_pos/widgets/top_nav_admin.dart';

class AdminLayout extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBodyBehindAppBar: false,
      appBar: topNavigationBarAdmin(context, scaffoldKey),
      drawer: Drawer(child: SideMenu(),),
      body: ResponsiveWidget(
        largeScreen: LargeScreenAdmin(),
        smallScreen: SmallScreenAdmin(),
        mediumScreen: LargeScreenAdmin(),
      ),
    );
  }
}
