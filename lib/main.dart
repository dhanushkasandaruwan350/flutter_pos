import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/controllers/menu_controller.dart';
import 'package:pizza_pos/controllers/navigation_controller.dart';
import 'package:pizza_pos/pages/common/authentication/authentication.dart';

import 'admin_layout.dart';

void main() {
  Get.put(MenuController());
  Get.put(NavigationController());
  runApp(PizzaPos());
}

class PizzaPos extends StatelessWidget {
  const PizzaPos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Pizza Shop",
      theme: ThemeData(
          scaffoldBackgroundColor: light,
          textTheme: GoogleFonts.mulishTextTheme(Theme.of(context).textTheme)
              .apply(bodyColor: Colors.black),
          pageTransitionsTheme: PageTransitionsTheme(builders: {
            TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
            TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
          }),
          primaryColor: Colors.blue),
      home: AdminLayout(),
    );
  }
}
