import 'package:flutter/widgets.dart';
import 'package:pizza_pos/constants/controllers.dart';
import 'package:pizza_pos/routing/router.dart';
import 'package:pizza_pos/routing/routes.dart';

Navigator localNavigator = Navigator(
  key: navigationController.navigationKey,
  initialRoute: OverViewPageRoute,
  onGenerateRoute: generateRoute,
);
