import 'package:flutter/material.dart';
import 'package:pizza_pos/pages/admin/customers/customers.dart';
import 'package:pizza_pos/pages/admin/drivers/driver.dart';
import 'package:pizza_pos/pages/admin/overview/overview.dart';
import 'package:pizza_pos/pages/common/authentication/authentication.dart';

import 'routes.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case OverViewPageRoute:
      return _getPageRoute(OverViewPage());
    case DriversPageRoute:
      return _getPageRoute(DriverPage());
    case CustomersPageRoute:
      return _getPageRoute(CustomerPage());
    case AuthenticationPageRoute:
      return _getPageRoute(AuthenticationPage());
    default:
      return _getPageRoute(OverViewPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(builder: (context) => child);
}
