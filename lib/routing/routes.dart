const OverViewPageRoute = "Overview";
const DriversPageRoute = "Drivers";
const CustomersPageRoute = "Customers";
const AuthenticationPageRoute = "Authentication";

List sideMenuItems = [
  OverViewPageRoute,
  DriversPageRoute,
  CustomersPageRoute,
  AuthenticationPageRoute,
];
