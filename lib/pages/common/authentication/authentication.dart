import 'package:flutter/material.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/widgets/custom_text.dart';

class AuthenticationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(236,240,245,5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
              text: "Account Login",
              size: 15,
              color: dark,
              weight: FontWeight.bold)
        ],
      ),
    );
  }
}
