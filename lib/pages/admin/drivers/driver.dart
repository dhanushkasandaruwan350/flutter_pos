import 'package:flutter/material.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/widgets/custom_text.dart';

class DriverPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CustomText(text: "Driver",color: dark,weight: FontWeight.bold,size: 20,),
    );
  }
}
