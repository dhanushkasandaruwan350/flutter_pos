import 'package:flutter/material.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/widgets/custom_text.dart';

class CustomerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CustomText(text: "Customer",color: dark,weight: FontWeight.bold,size: 20,),
    );
  }
}
