 import 'package:flutter/material.dart';
import 'package:pizza_pos/helpers/local_navigator.dart';

class SmallScreenAdmin extends StatelessWidget {
   const SmallScreenAdmin({Key? key}) : super(key: key);

   @override
   Widget build(BuildContext context) {
     return localNavigator;
   }
 }
