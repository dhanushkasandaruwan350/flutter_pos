import 'package:flutter/material.dart';
import 'package:pizza_pos/helpers/local_navigator.dart';
import 'package:pizza_pos/widgets/side_menu.dart';

class LargeScreenAdmin extends StatelessWidget {
  const LargeScreenAdmin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: SideMenu(key: key,)),
        Expanded(
            flex: 5,
            child: localNavigator),
      ],
    );
  }
}
