import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pizza_pos/constants/controllers.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/helpers/responsiveness.dart';
import 'package:pizza_pos/routing/routes.dart';

import 'custom_text.dart';

class VerticalMenuItem extends StatelessWidget {
  final String itemName;
  final Function onTap;

  const VerticalMenuItem(
      {Key? key, required this.itemName, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (itemName == AuthenticationPageRoute) {
          //TODO:to loogin page
        }
        if (!menuController.isActive(itemName)) {
          menuController.changeActiveItemTo(itemName);
          navigationController.navigateTo(itemName);
          if (ResponsiveWidget.isSmallScreen(context)) {
            Get.back();
          }
        }
      },
      onHover: (value) {
        value
            ? menuController.onHover(itemName)
            : menuController.onHover("not hovering");
      },
      child: Obx(() => Container(
            color: menuController.isHovering(itemName)
                ? lightGray.withOpacity(.1)
                : Colors.transparent,
            child: Row(
              children: [
                Visibility(
                  visible: menuController.isHovering(itemName) ||
                      menuController.isActive(itemName),
                  child: Container(
                    width: 3,
                    height: 72,
                    color: dark,
                  ),
                  maintainSize: true,
                  maintainState: true,
                  maintainAnimation: true,
                ),
                Expanded(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(16),
                      child: menuController.returnIconFor(itemName),
                    ),
                    if (!menuController.isActive(itemName))
                      Flexible(
                          child: CustomText(
                              text: itemName,
                              size: 16,
                              color: menuController.isHovering(itemName)
                                  ? dark
                                  : lightGray,
                              weight: FontWeight.normal))
                    else
                      Flexible(
                        child: CustomText(
                            text: itemName,
                            size: 18,
                            color: dark,
                            weight: FontWeight.bold),
                      )
                  ],
                )),
              ],
            ),
          )),
    );
  }
}
