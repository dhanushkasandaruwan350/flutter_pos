import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pizza_pos/constants/controllers.dart';
import 'package:pizza_pos/constants/styles.dart';
import 'package:pizza_pos/helpers/responsiveness.dart';
import 'package:pizza_pos/routing/routes.dart';
import 'package:pizza_pos/widgets/custom_text.dart';
import 'package:pizza_pos/widgets/side_menu_item.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Container(
      color: light,
      child: ListView(
        children: [
          if (ResponsiveWidget.isSmallScreen(context))
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 40,
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 12),
                      child: Image.asset(
                        "assets/icons/logo.png",
                        width: 28,
                      ),
                    ),
                    Flexible(
                      child: CustomText(
                        text: "Dash",
                        color: active,
                        size: 20,
                        weight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: _width / 48,
                    ),
                  ],
                ),
              ],
            ),
          SizedBox(
            height: 40,
          ),
          Divider(
            color: lightGray.withOpacity(.1),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: sideMenuItems
                .map((itemName) => SideMenuItem(
                      itemName: itemName == AuthenticationPageRoute
                          ? "Log Out"
                          : itemName,
                      onTap: () {

                      },
                    ))
                .toList(),
          ),
        ],
      ),
    );
  }
}
